/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

#ifndef __ZZ9000_H
#define __ZZ9000_H

#include <linux/zorro.h>
#include "zz9000_regs.h"

/* TODO: Move to zorro_ids.h */
#define ZORRO_MANUF_MNT	0x6d6e
#define   ZORRO_PROD_MNT_ZZ9000_ZII  ZORRO_ID(MNT, 0x03, 0)
#define   ZORRO_PROD_MNT_ZZ9000_ZIII ZORRO_ID(MNT, 0x04, 0)

#define ZZ9000_VERSION "0.1.5.1 beta"
struct zz9000_platform_data {
	void __iomem *registers;
	void __iomem *eth_rx_mem;
	void __iomem *eth_tx_mem;
	void __iomem *usb_mem;
	void __iomem *fb_mem;
	struct resource *res[5];
	struct zorro_dev *zdev;
	struct fb_info *fb_info;
	struct device *sysfs_root;
	int fb_size;
};

struct zz9000_platform_data *get_zz9000_data(void);

static inline int is_zorro3(struct zorro_dev *zdev)
{
	if (zdev->id == ZORRO_PROD_MNT_ZZ9000_ZIII)
		return 1;

	return 0;
}

#define zz_readw(x) (z_readw(zz9000_data->registers + (x)))
#define zz_readl(x) (z_readl(zz9000_data->registers + (x)))
#define zz_writew(v, x) z_writew((v), zz9000_data->registers + (x))
#define zz_writel(v, x) z_writel((v), zz9000_data->registers + (x))

#endif
