/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

#include <drm/drm_fourcc.h>
#include <linux/console.h>
#include <linux/fb.h>
#include <linux/module.h>
#include <linux/zorro.h>
#include <asm/amigahw.h>
#include <asm/amigaints.h>
#include "zz9000.h"

#define PSEUDO_PALETTE_SIZE 16
struct zz9000fb_par {
	u32 palette[PSEUDO_PALETTE_SIZE];
};

static const struct fb_fix_screeninfo zz9000fb_fix = {
	.id = "ZZ9000",
	.type = FB_TYPE_PACKED_PIXELS,
	.visual = FB_VISUAL_TRUECOLOR,
	.accel = FB_ACCEL_NONE,
};

static const struct fb_var_screeninfo zz9000fb_var = {
	.height = -1,
	.width = -1,
	.activate = FB_ACTIVATE_NOW,
	.vmode = FB_VMODE_NONINTERLACED,
};

struct zz9000fb_format {
	const char *name;
	u32 bits_per_pixel;
	struct fb_bitfield red;
	struct fb_bitfield green;
	struct fb_bitfield blue;
	struct fb_bitfield transp;
	u32 fourcc;
};

struct zz9000fb_format zz9000fb_formats[3] = {
	{ "16bit RGB(5:6:5)",    16, {11, 5}, {5, 6}, {0, 5}, { 0, 0}, DRM_FORMAT_RGB565   },
	{ "15bit RGB(5:5:5)",    16, {10, 5}, {5, 5}, {0, 5}, { 0, 0}, DRM_FORMAT_XRGB1555 },
	{ "32bit BGRA(8:8:8:8)", 32, {16, 8}, {8, 8}, {24, 8}, {0, 8}, DRM_FORMAT_BGRA8888 },
};

#define MHz * 1024 * 1024
static struct fb_videomode zz9000fb_modedb[] __initdata = {
	{ "1280x720p60", 60, 1280, 720, 13426, 192, 64, 22, 1, 136, 3,
	  FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED },
	{ "800x600p60", 60, 800, 600, 25000, 88, 40, 23, 1, 128, 4,
	  FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED },
	{ "640x480p60", 60, 640, 480, 39682,  48, 16, 33, 10, 96, 2,
	  FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED },
	{ "640x512p60", 60, 640, 512, 41667, 113, 87, 18, 1, 56, 3, // fake, values are @50Hz
	  FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED },
	{ "720x480p60", 60, 720, 480, 37037, 68, 12, 39, 5, 64, 5,
	  FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED },
	{ "1024x768p60", 60, 1024, 768, 15384, 160, 24, 29, 3, 136, 6,
	  FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED },
	{ "1280x1024p60", 60, 1280, 1024, 9259, 248, 48, 38, 1, 112, 3,
	  FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED },
	{ "1920x1080p50", 50, 1920, 1080, 6734, 148, 528, 36, 4, 44, 5,
	  FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED },
	{ "1920x1080p60", 60, 1920, 1080, 5787, 328, 120, 34, 1, 208, 3,
	  FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED },
	{ "720x576p50", 50, 720, 576, 37037, 68, 12, 39, 5, 64, 5,
	  FB_SYNC_BROADCAST, FB_VMODE_NONINTERLACED }
};

#define NUM_TOTAL_MODES  ARRAY_SIZE(zz9000fb_modedb)
static int defmode = 0;
#define DEFAULT_BPP 16

struct zz9000_platform_data *zz9000_data;
static char *mode = NULL;
static int zz9000_accel = 0;
static int mntzz_vmode = MNTZZ_MODE_1280x720, mntzz_cmode = MNTZZ_MODE_COLOR_16BIT;
static struct zz9000fb_format *format = NULL;

static int zz9000fb_setcolreg(unsigned regno, unsigned red, unsigned green,
			      unsigned blue, unsigned transp,
			      struct fb_info *info)
{
	u32 *pal = info->pseudo_palette;
	u32 cr = red >> (16 - info->var.red.length);
	u32 cg = green >> (16 - info->var.green.length);
	u32 cb = blue >> (16 - info->var.blue.length);
	u32 value;

	if (regno >= PSEUDO_PALETTE_SIZE)
		return -EINVAL;

	value = (cr << info->var.red.offset) |
	    (cg << info->var.green.offset) | (cb << info->var.blue.offset);
	if (info->var.transp.length > 0) {
		u32 mask = (1 << info->var.transp.length) - 1;
		mask <<= info->var.transp.offset;
		value |= mask;
	}
	pal[regno] = value;

	return 0;
}


/* fb_try_mode says: Returns 1 on success.
 * However, all the drivers I looked at return -EINVAL or 0.
 */
static int zz9000fb_check_var(struct fb_var_screeninfo *var,
			      struct fb_info *info)
{
	int bytes = 0;

	mntzz_vmode = -1;
	mntzz_cmode = -1;

	if (var->xres == 1280 && var->yres == 720)
		mntzz_vmode = MNTZZ_MODE_1280x720;
	if (var->xres == 800 && var->yres == 600)
		mntzz_vmode = MNTZZ_MODE_800x600;
	if (var->xres == 640 && var->yres == 480)
		mntzz_vmode = MNTZZ_MODE_640x480;
	if (var->xres == 1024 && var->yres == 768)
		mntzz_vmode = MNTZZ_MODE_1024x768;
	if (var->xres == 1280 && var->yres == 1024)
		mntzz_vmode = MNTZZ_MODE_1280x1024;
	if (var->xres == 1920 && var->yres == 1080)
		if (var->bits_per_pixel <= 16)
			mntzz_vmode = MNTZZ_MODE_1920x1080;
	if (var->xres == 720 && var->yres == 576)
		mntzz_vmode = MNTZZ_MODE_720x576p50;
	// FIXME MNTZZ_MODE_1920x1080p50 do we know refresh here?
	if (var->xres == 720 && var->yres == 480)
		mntzz_vmode = MNTZZ_MODE_720x480;
	if (var->xres == 640 && var->yres == 512)
		mntzz_vmode = MNTZZ_MODE_640x512;

	if (mntzz_vmode == -1)
		return -EINVAL;

	switch (var->bits_per_pixel) {
	case 8:
		// Not yet implemented
		break;
	case 15:
		bytes = 2;
		mntzz_cmode = MNTZZ_MODE_COLOR_15BIT;
		format = &zz9000fb_formats[1];
		break;
	case 16:
		bytes = 2;
		mntzz_cmode = MNTZZ_MODE_COLOR_16BIT;
		format = &zz9000fb_formats[0];
		break;
	case 32:
		bytes = 4;
		mntzz_cmode = MNTZZ_MODE_COLOR_32BIT;
		format = &zz9000fb_formats[2];
		break;
	default:
		// Nothing to do
		break;
	}

	// TODO on ZII, check if mode requires more memory than we mapped

	if (mntzz_cmode == -1)
		return -EINVAL;

	var->red = format->red;
	var->green = format->green;
	var->blue = format->blue;
	var->transp = format->transp;

	info->fix.line_length = bytes * var->xres;

	return 0;
}

/* 0 unblank, 1 blank, 2 no vsync, 3 no hsync, 4 off */

static int zz9000fb_blank(int blank, struct fb_info *info)
{
	pr_debug("zz9000fb: %sblank.\n", blank ? "" : "un");
	return 0;
}

static void zz9000fb_fill_rectangle(struct fb_info *info, uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint32_t color)
{
	zz_writew(x, MNTZZ_BLITTER_X1);
	zz_writew(y, MNTZZ_BLITTER_Y1);
	zz_writew(w, MNTZZ_BLITTER_X2);
	zz_writew(h, MNTZZ_BLITTER_Y2);
	zz_writew(info->fix.line_length >> 2, MNTZZ_BLITTER_ROW_PITCH);
	zz_writew(mntzz_cmode, MNTZZ_BLITTER_COLORMODE);
	zz_writew(color >> 16, MNTZZ_BLITTER_RGB_HI);
	zz_writew(color & 0xffff, MNTZZ_BLITTER_RGB_LO);
	zz_writew(0x00ff, MNTZZ_BLITTER_OP_FILLRECT);
}
static void zz9000fb_fillrect(struct fb_info *info,
			      const struct fb_fillrect *region)
{
	if (zz9000_accel) {
		if (region->rop == ROP_COPY)
			zz9000fb_fill_rectangle(info, region->dx, region->dy,
				region->width, region->height, region->color);
		else
			cfb_fillrect(info, region);
	} else
		cfb_fillrect(info, region);
}

static void zz9000fb_copyarea(struct fb_info *info,
			      const struct fb_copyarea *area)
{
	if (zz9000_accel) {
		zz_writew(area->dx, MNTZZ_BLITTER_X1);
		zz_writew(area->dy, MNTZZ_BLITTER_Y1);
		zz_writew(area->width, MNTZZ_BLITTER_X2);
		zz_writew(area->height, MNTZZ_BLITTER_Y2);

		zz_writew(area->sx, MNTZZ_BLITTER_X3);
		zz_writew(area->sy, MNTZZ_BLITTER_Y3);
		zz_writew(info->fix.line_length >> 2, MNTZZ_BLITTER_ROW_PITCH);
		zz_writew(0xff00 | mntzz_cmode, MNTZZ_BLITTER_COLORMODE);
		zz_writew(0x0001, MNTZZ_BLITTER_OP_COPYRECT);
	} else
		cfb_copyarea(info, area);
}

static void
zz9000fb_imageblit(struct fb_info *info, const struct fb_image *image)
{
	// TODO accelerated implementation
	cfb_imageblit(info, image);
	return;
}

void zz9000fb_fix_vsync(void)
{
	/* The AmigaOS driver does this 100x */
	int i;
	for (i = 0; i < 100; i++) {
		zz_writew(0x0000, MNTZZ_VIDEO_CTRL_DATA_HI);
		zz_writew(0x0001, MNTZZ_VIDEO_CTRL_DATA_LO);
		zz_writew(MNTZZ_OP_VSYNC, MNTZZ_VIDEO_CTRL_OP);
		zz_writew(MNTZZ_OP_NOP, MNTZZ_VIDEO_CTRL_OP);
		zz_writew(0x0000, MNTZZ_VIDEO_CTRL_DATA_LO);
	}
}

void zz9000fb_setmode(struct fb_info *info)
{
	zz_writew(MNTZZ_CAPTURE_OFF, MNTZZ_VIDEO_CAPTURE_MODE);
	zz_writew(0x0000, MNTZZ_PAN_PTR_HI);
	zz_writew(0x0000, MNTZZ_PAN_PTR_LO);
	zz_writew(0x0000, MNTZZ_BLITTER_SRC_HI);
	zz_writew(0x0000, MNTZZ_BLITTER_SRC_LO);
	zz_writew(0x0000, MNTZZ_BLITTER_DST_HI);
	zz_writew(0x0000, MNTZZ_BLITTER_DST_LO);

	zz_writew(MNTZZ_MODE_SCALE_0 | mntzz_cmode | mntzz_vmode,
		  MNTZZ_MODE);

	zz9000fb_fix_vsync();

	zz9000fb_fill_rectangle(info, 0,0, info->var.xres, info->var.yres, 0xaaaaaa); // 0xad55?
}

static void
zz9000_capture_mode(uint32_t capture_mode)
{
	uint16_t panPtrHi;
	uint16_t panPtrLo;
	uint16_t scaleMode;
	uint16_t colorMode;
	uint16_t dispMode;


	switch (capture_mode) {
		case MNTZZ_VMODE_720x576:
			panPtrHi	= MNTZZ_CAPTURE_PAN_PAL >> 16;
			panPtrLo	= MNTZZ_CAPTURE_PAN_PAL & 0xffff;
			dispMode	= MNTZZ_MODE_720x576p50;
			break;
		case MMNTZ_VMODE_800x600:
			panPtrHi	= MNTZZ_CAPTURE_PAN_VGA >> 16;
			panPtrLo	= MNTZZ_CAPTURE_PAN_VGA & 0xffff;
			dispMode	= MNTZZ_MODE_800x600;
			break;
		default:
			panPtrHi	= MNTZZ_CAPTURE_PAN_PAL >> 16;
			panPtrLo	= MNTZZ_CAPTURE_PAN_PAL & 0xffff;
			dispMode	= MNTZZ_MODE_720x576p50;
			break;
	}
	colorMode = MNTZZ_MODE_COLOR_32BIT;
	scaleMode = MNTZZ_MODE_SCALE_2;

	zz_writew(panPtrHi, MNTZZ_PAN_PTR_HI);
	zz_writew(panPtrLo, MNTZZ_PAN_PTR_LO);
	zz_writew(dispMode, MNTZZ_VIDEOCAP_VMODE);
	zz_writew(scaleMode | colorMode | dispMode, MNTZZ_MODE);
	zz_writew(MNTZZ_CAPTURE_ON, MNTZZ_VIDEO_CAPTURE_MODE);
	zz9000fb_fix_vsync();
}

static int zz9000fb_set_par(struct fb_info *info)
{
	zz9000fb_setmode(info);

	dev_info(&zz9000_data->zdev->dev,
		 "framebuffer at 0x%lx, 0x%x bytes, mapped to 0x%p\n",
		 info->fix.smem_start, info->fix.smem_len, info->screen_base);
	dev_info(&zz9000_data->zdev->dev, "format=%s, mode=%dx%dx%d, linelength=%d\n",
		 format->name, info->var.xres, info->var.yres,
		 info->var.bits_per_pixel, info->fix.line_length);
	return 0;
}

static struct fb_ops zz9000fb_ops = {
	.owner        = THIS_MODULE,
	.fb_blank     = zz9000fb_blank,
	.fb_check_var = zz9000fb_check_var,
	.fb_setcolreg = zz9000fb_setcolreg,
	.fb_set_par   = zz9000fb_set_par,
	.fb_fillrect  = zz9000fb_fillrect,
	.fb_copyarea  = zz9000fb_copyarea,
	.fb_imageblit = zz9000fb_imageblit,
};

static int __init zz9000fb_setup(char *options)
{
	char *this_opt;

	if (!options || !*options)
		return 0;

	while ((this_opt = strsep(&options, ",")) != NULL) {
		if (!*this_opt)
			continue;
		/* Not much here yet */
		if (!strcmp(this_opt, "noaccel")) {
			zz9000_accel = 0;
		} else
			mode = this_opt;
	}
	return 0;
}

static int __init zz9000_graphics_init(struct zz9000_platform_data *data)
{
	int ret;

	struct fb_info *info;
	struct zz9000fb_par *par;
	char *option = NULL;
	uint32_t board = data->zdev->resource.start;

	zz9000_data = data;

	if (fb_get_options("zz9000fb", &option))
		return -ENODEV;
	zz9000fb_setup(option);

	info = framebuffer_alloc(sizeof(struct zz9000fb_par), &data->zdev->dev);
	if (!info)
		return -ENOMEM;
	data->fb_info = info;

	par = info->par;

	info->fix = zz9000fb_fix;
	info->fix.smem_start = board + 0x10000;
	info->fix.smem_len = is_zorro3(data->zdev) ? 0x500000 : 0x3f0000;
	info->fix.mmio_start = board;
	info->fix.mmio_len = 0x2000;

	info->var = zz9000fb_var;

	info->apertures = alloc_apertures(1);
	if (!info->apertures) {
		ret = -ENOMEM;
		goto error_fb_release;
	}
	info->apertures->ranges[0].base = info->fix.smem_start;
	info->apertures->ranges[0].size = info->fix.smem_len;

	info->fbops = &zz9000fb_ops;
	info->flags = FBINFO_DEFAULT;
	info->screen_base = data->fb_mem;

	info->pseudo_palette = par->palette;

	if (!fb_find_mode(&info->var, info, mode, zz9000fb_modedb,
			  NUM_TOTAL_MODES, &zz9000fb_modedb[defmode], DEFAULT_BPP))
		return -EINVAL;

	fb_videomode_to_modelist(zz9000fb_modedb, NUM_TOTAL_MODES,
				 &info->modelist);

	ret = register_framebuffer(info);
	if (ret < 0) {
		dev_err(&data->zdev->dev, "Unable to register zz9000fb: %d\n",
			ret);
		goto error_fb_release;
	}

	dev_info(&data->zdev->dev, "fb%d: zz9000fb registered!\n", info->node);

	return 0;

error_fb_release:
	framebuffer_release(info);
	return ret;
}

int zz9000_graphics_remove(struct zz9000_platform_data *data)
{
	zz9000_capture_mode(MNTZZ_VMODE_720x576);
	unregister_framebuffer(data->fb_info);
	framebuffer_release(data->fb_info);
	data->fb_info = NULL;

	return 0;
}

static int __init zz9000_graphics_module_init(void)
{
	struct zz9000_platform_data *data;

	data = get_zz9000_data();
	return zz9000_graphics_init(data);
}

module_init(zz9000_graphics_module_init);

static void __exit zz9000_graphics_module_exit(void)
{
	struct zz9000_platform_data *data;

	data = get_zz9000_data();
	zz9000_graphics_remove(data);
}

module_exit(zz9000_graphics_module_exit);

module_param(mode, charp, 0);
MODULE_PARM_DESC(mode,
		 "\nSelects the desired default display mode in the format XxYxDepth,\n"
		 "eg. 1024x768x16. Other formats supported include XxY-Depth and\n"
		 "XxY-Depth@Rate. (default: 1280x720-60@16)\n");

MODULE_DESCRIPTION("MNT ZZ9000 graphics driver");
MODULE_AUTHOR("Stefan Reinauer <stefan.reinauer@coreboot.org>");
MODULE_LICENSE("GPL");
