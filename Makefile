
obj-m       += zz9000.o zz9000_network.o zz9000_usb.o zz9000_graphics.o
zz9000-y    := zz9000_core.o

ARCH=$(shell uname -p)
ifneq ($(ARCH),m68k)
export KROOT=/usr/src/linux-source-5.4
MAKEOPTS=ARCH=m68k CROSS_COMPILE=m68k-linux-gnu-
else
export KROOT=/lib/modules/$(shell uname -r)/build
endif


allofit:  modules copy
modules:
	@$(MAKE) $(MAKEOPTS) -C $(KROOT) M=$(PWD) modules
modules_install:
	@$(MAKE) $(MAKEOPTS) -C $(KROOT) M=$(PWD) modules_install
kernel_clean:
	@$(MAKE) $(MAKEOPTS) -C $(KROOT) M=$(PWD) clean

clean: kernel_clean
	rm -rf   Module.symvers modules.order

FILES := Makefile zz9000.h zz9000_core.c zz9000_usb.c zz9000_network.c zz9000_graphics.c
FILES += zz9000.ko zz9000_network.ko zz9000_usb.ko zz9000_graphics.ko

copy:
	cp $(FILES) /storage/stepan/amiga-drivers/
	@cd /storage/stepan; tar cf ~/amiga.tar amiga-drivers; cd -
	@#echo scp $(FILES) root@192.168.0.40:amiga-drivers/

