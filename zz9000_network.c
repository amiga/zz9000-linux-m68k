/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

#include <linux/console.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/module.h>
#include <linux/zorro.h>
#include <asm/amigahw.h>
#include <asm/amigaints.h>
#include "zz9000.h"

static struct zz9000_platform_data *zz9000_data;

/* Length definitions */
static unsigned char zz9000_mac_addr[ETH_ALEN];

/* net_device referencing */
static struct net_device *device;
static struct net_device_stats *stats;

/* priv structure that holds the informations about the device. */
struct eth_priv {
	spinlock_t lock;
	struct net_device *dev;
};

static void zz9000_hw_tx(char *data, int len, struct net_device *dev)
{
	void *zz9000_tx_mem = zz9000_data->eth_tx_mem;
	uint16_t ret;

	memcpy(zz9000_tx_mem, data, len);

#if 0
	netdev_dbg(dev, "send data=%p len=%d\n", data, len);

	print_hex_dump_debug("eth: ", DUMP_PREFIX_OFFSET,
		       16, 1, data, len, true);
#endif
	zz_writew(len, MNTZZ_ETH_TX);
	ret = zz_readw(MNTZZ_ETH_TX);
	if (ret != 0)
		netdev_dbg(dev, "ret = %d\n", ret);
}

static uint32_t old_serial = 0;

/*
 * Read a packet out of the adapter and pass it to the upper layers
 */
static inline int recv_packet(struct net_device *dev)
{
	unsigned short pktlen;
	struct sk_buff *skb;

	uint8_t *frame = (uint8_t *) zz9000_data->eth_rx_mem;
	uint32_t ser = ((uint32_t) frame[2] << 8) | ((uint32_t) frame[3]);
	pktlen = ((uint32_t) frame[0] << 8) | ((uint32_t) frame[1]);

	/* There is not interrupt status register for network,
	 * so we use the serial number of the frame to determine
	 * whether there's a new packet
	 */
	if (old_serial == ser)
		return 0;
	old_serial = ser;

	if (!pktlen) {
		netdev_dbg(dev, "%s: pktlen == 0\n", __func__);
		dev->stats.rx_errors++;
		return -1;
	}

	skb = dev_alloc_skb(pktlen + 2);
	if (!skb) {
		netdev_dbg(dev, "%s: out of mem (buf_alloc failed)\n",
			   __func__);
		dev->stats.rx_dropped++;
		return -1;
	}

	skb->dev = dev;
#if 0
	skb_reserve(skb, NET_IP_ALIGN);
	skb_put(skb, pktlen);	/* make room */

	//print_hex_dump_debug("eth: ", DUMP_PREFIX_OFFSET,
	//	       16, 1, frame, pktlen, true);

	// Copy buffer to skb
	memcpy(skb->data, frame + 4, pktlen);
#else
	memcpy(skb_put(skb, pktlen), frame + 4, pktlen);
#endif


	/* Mark this frame as accepted */
	zz_writew(0x0001, MNTZZ_ETH_RX);

	skb->protocol = eth_type_trans(skb, dev);
	skb->ip_summed = CHECKSUM_NONE;
	dev->stats.rx_packets++;
	dev->stats.rx_bytes += pktlen;

	netif_rx(skb);

	/* and enqueue packet */
	return 1;
}

static irqreturn_t zz9000_eth_interrupt(int irq, void *dev_id)
{
	struct eth_priv *priv;
	int rcv = 1;
	struct net_device *dev = (struct net_device *)dev_id;

	/* paranoid */
	if (!dev)
		return IRQ_HANDLED;

	/* Lock the device */
	priv = netdev_priv(dev);
	spin_lock(&priv->lock);

	/* Fetch all available packets. */
	while (rcv) {
		rcv = recv_packet(dev);
	}

	/* Unlock the device and we are done */
	spin_unlock(&priv->lock);

	return IRQ_HANDLED;
}

static int zz9000_eth_open(struct net_device *dev)
{
	int ret;

	/* Install interrupt handler for IRQ6.
	 * IRQ2 is currently not supported.
	 */
	ret = request_irq(IRQ_AMIGA_EXTER, zz9000_eth_interrupt, IRQF_SHARED,
			  "ZZ9000 Ethernet", dev);
	if (ret)
		return ret;

	/* Enable Interrupts on ZZ9000 */
	zz_writew(zz_readw(MNTZZ_CONFIG) | 0x01, MNTZZ_CONFIG);

	/* start up the transmission queue */
	netif_start_queue(dev);
	return 0;
}

static int zz9000_eth_close(struct net_device *dev)
{
	/* Disable interrupts on ZZ9000 */
	zz_writew(zz_readw(MNTZZ_CONFIG) & 0xfffe, MNTZZ_CONFIG);

	/* shutdown the transmission queue */
	netif_stop_queue(dev);

	/* Remove interrupt handler */
	free_irq(IRQ_AMIGA_EXTER, dev);
	return 0;
}

static int zz9000_eth_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	int len;
	char *data, shortpkt[ETH_ZLEN];

	data = skb->data;
	len = skb->len;
	if (len < ETH_ZLEN) {
		memset(shortpkt, 0, ETH_ZLEN);
		memcpy(shortpkt, skb->data, skb->len);
		len = ETH_ZLEN;
		data = shortpkt;
	}

	/* Send that packet out! */
	zz9000_hw_tx(data, len, dev);

	dev->stats.tx_packets++;
	dev->stats.tx_bytes += len;

	dev_kfree_skb(skb);

	return NETDEV_TX_OK;
}

static int zz9000_eth_do_ioctl(struct net_device *dev, struct ifreq *ifr,
			       int cmd)
{
	printk(KERN_INFO "zz9000_eth_do_ioctl(%s): 0x%x\n", dev->name, cmd);
	return -1;
}

static struct net_device_stats *zz9000_eth_get_stats(struct net_device *dev)
{
	// printk(KERN_INFO "zz9000_eth_get_stats(%s)\n", dev->name);
	return stats;
}

/*
 * This is where ifconfig comes down and tells us who we are, etc.
 * We can just ignore this.
 */
static int zz9000_eth_config(struct net_device *dev, struct ifmap *map)
{
	printk("zz9000_eth_config(%s)\n", dev->name);
	if (dev->flags & IFF_UP) {
		return -EBUSY;
	}
	return 0;
}

static int zz9000_eth_change_mtu(struct net_device *dev, int new_mtu)
{
	unsigned long flags = 0;
	struct eth_priv *priv = netdev_priv(dev);
	spinlock_t *lock = &priv->lock;

	printk(KERN_INFO "zz9000_eth_change_mtu(%s)\n", dev->name);

	/* Check ranges */
	if ((new_mtu < 68) || (new_mtu > 10000))	// Remember to see at the hardware documentation the right especification
		return -EINVAL;

	spin_unlock_irqrestore(lock, flags);
	printk(KERN_INFO "Old mtu: (%d) New mtu: (%d)", dev->mtu, new_mtu);
	dev->mtu = new_mtu;
	spin_unlock_irqrestore(lock, flags);
	return 0;		/* Sucess */
}

static void zz9000_eth_tx_timeout(struct net_device *dev, unsigned int tx_queue)
{
	dev->stats.tx_errors++;

	netif_wake_queue(dev);
	return;
}

/*
 * Structure that holds all the options supported by the driver.
 */
static struct net_device_ops zz9000_netdev_ops = {
	.ndo_open = zz9000_eth_open,
	.ndo_stop = zz9000_eth_close,
	.ndo_start_xmit = zz9000_eth_start_xmit,
	.ndo_do_ioctl = zz9000_eth_do_ioctl,
	.ndo_get_stats = zz9000_eth_get_stats,
	.ndo_set_config = zz9000_eth_config,
	.ndo_change_mtu = zz9000_eth_change_mtu,
	.ndo_tx_timeout = zz9000_eth_tx_timeout,
	// Does NAPI even make sense for this device?
	//.ndo_poll_controller = zz9000_eth_napi_poll;
};

static void zz9000_eth_setup(struct net_device *dev)
{
	zz9000_mac_addr[0] = zz_readw(MNTZZ_ETH_MAC_HI) >> 8;
	zz9000_mac_addr[1] = zz_readw(MNTZZ_ETH_MAC_HI) & 0xff;
	zz9000_mac_addr[2] = zz_readw(MNTZZ_ETH_MAC_MD) >> 8;
	zz9000_mac_addr[3] = zz_readw(MNTZZ_ETH_MAC_MD) & 0xff;
	zz9000_mac_addr[4] = zz_readw(MNTZZ_ETH_MAC_LO) >> 8;
	zz9000_mac_addr[5] = zz_readw(MNTZZ_ETH_MAC_LO) & 0xff;

	ether_setup(dev);

	dev->netdev_ops = &zz9000_netdev_ops;
	dev->irq = IRQ_AMIGA_EXTER;

	dev->hard_header_len = ETH_HLEN;
	dev->mtu = ETH_DATA_LEN;
	dev->addr_len = ETH_ALEN;
	dev->flags |= IFF_BROADCAST | IFF_MULTICAST;
	memset(dev->broadcast, 0xFF, ETH_ALEN);
	memcpy(dev->dev_addr, zz9000_mac_addr, ETH_ALEN);

	stats = &dev->stats;
}

int zz9000_network_init(struct zz9000_platform_data *data)
{
	int ret = 0;

	zz9000_data = data;

	/* Allocating the net device. */
	device = alloc_netdev(0, "eth%d", NET_NAME_UNKNOWN, zz9000_eth_setup);

	if ((ret = register_netdev(device))) {
		printk(KERN_EMERG
		       "zz9000: net: error %i registering  device \"%s\"\n", ret,
		       device->name);
		free_netdev(device);
		return -1;
	}

	printk(KERN_INFO "  ZZ9000 Ethernet %s MAC ADDR: %02x:%02x:%02x:%02x:%02x:%02x\n",
	       dev_name(&device->dev),
	       zz9000_mac_addr[0], zz9000_mac_addr[1], zz9000_mac_addr[2],
	       zz9000_mac_addr[3], zz9000_mac_addr[4], zz9000_mac_addr[5]);

	return 0;
}

int zz9000_network_remove(struct zz9000_platform_data *data)
{
	if (device) {
		unregister_netdev(device);
		free_netdev(device);
		printk(KERN_INFO "zz9000: Network device removed.\n");
	}

	return 0;
}

static int __init zz9000_network_module_init(void)
{
	struct zz9000_platform_data *data = get_zz9000_data();
	return zz9000_network_init(data);
}

module_init(zz9000_network_module_init);

static void __exit zz9000_network_module_exit(void)
{
	struct zz9000_platform_data *data = get_zz9000_data();
	zz9000_network_remove(data);
}

module_exit(zz9000_network_module_exit);

MODULE_DESCRIPTION("MNT ZZ9000 network driver");
MODULE_AUTHOR("Stefan Reinauer <stefan.reinauer@coreboot.org>");
MODULE_LICENSE("GPL");
