/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

#include <linux/blk-mq.h>
#include <linux/console.h>
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/string_helpers.h>
#include <uapi/linux/cdrom.h>
#include <uapi/linux/hdreg.h>
#include <asm/div64.h>
#include <asm/zorro.h>

#include "zz9000.h"

static struct zz9000_platform_data *zz9000_data;

typedef struct {
	struct zz9000_platform_data *data;
	struct request_queue *queue;
	struct gendisk *disk;
	atomic_t open_counter;
	sector_t capacity;
	struct blk_mq_tag_set tag_set;
} zz9000_usb_storage_t;

static int zz9000_usb_open(struct block_device *bdev, fmode_t mode)
{
	zz9000_usb_storage_t *dev = bdev->bd_disk->private_data;
	if (dev == NULL) {
		printk(KERN_WARNING "zz9000: USB: Invalid disk private_data in %s.\n", __func__);
		return -ENXIO;
	}

	atomic_inc(&dev->open_counter);

	pr_debug("zz9000: USB: Device was opened.\n");

	return 0;
}

static void zz9000_usb_release(struct gendisk *disk, fmode_t mode)
{
	zz9000_usb_storage_t *dev = disk->private_data;
	if (dev) {
		atomic_dec(&dev->open_counter);
		pr_debug("zz9000: USB: Device was closed.\n");
	} else
		printk(KERN_WARNING "zz9000: USB: Invalid disk private_data in %s.\n", __func__);
}

static int zz9000_usb_getgeo(zz9000_usb_storage_t * dev,
			     struct hd_geometry *geo)
{
	sector_t quotient;
	geo->start = 0;
	if (dev->capacity > 63) {
		geo->sectors = 63;
		quotient = div_u64((dev->capacity + (63 - 1)), 63);

		if (quotient > 255) {
			geo->heads = 255;
			geo->cylinders = (unsigned short)(div_u64((quotient + (255 - 1)), 255));
		} else {
			geo->heads = (unsigned char)quotient;
			geo->cylinders = 1;
		}
	} else {
		// Is this really needed?
		geo->sectors = (unsigned char)dev->capacity;
		geo->cylinders = 1;
		geo->heads = 1;
	}
	return 0;
}

static int zz9000_usb_ioctl(struct block_device *bdev, fmode_t mode,
			    unsigned int cmd, unsigned long arg)
{
	int ret = -ENOTTY;
	zz9000_usb_storage_t *dev = bdev->bd_disk->private_data;

	pr_debug("zz9000: USB: ioctl %x received\n", cmd);

	switch (cmd) {
	case HDIO_GETGEO:
		{
			struct hd_geometry geo;

			ret = zz9000_usb_getgeo(dev, &geo);
			if (copy_to_user
			    ((void *)arg, &geo, sizeof(struct hd_geometry)))
				ret = -EFAULT;
			else
				ret = 0;
			break;
		}

	case CDROM_GET_CAPABILITY:	/* 0x5331 - get capability */
		{
			struct gendisk *disk = bdev->bd_disk;

			if (bdev->bd_disk && (disk->flags & GENHD_FL_CD))
				ret = 0;
			else
				ret = -EINVAL;
			break;
		}

	default:
		{
			printk(KERN_WARNING
			       "zz9000: USB: ioctl %x not implemented.\n", cmd);
			ret = -EINVAL;
		}
	}

	return ret;
}

#ifdef CONFIG_COMPAT
static int zz9000_usb_compat_ioctl(struct block_device *bdev, fmode_t mode,
				   unsigned int cmd, unsigned long arg)
{
	// CONFIG_COMPAT is to allow running 32-bit userspace code on a 64-bit kernel
	return -ENOTTY;		// not supported
}
#endif

static const struct block_device_operations zz9000_usb_fops = {
	.owner = THIS_MODULE,
	.open = zz9000_usb_open,
	.release = zz9000_usb_release,
	.ioctl = zz9000_usb_ioctl,
#ifdef CONFIG_COMPAT
	.compat_ioctl = zz9000_usb_compat_ioctl,
#endif
};

typedef struct {
	//nothing
} zz9000_usb_cmd_t;

static int zz9000_usb_read(zz9000_usb_storage_t *dev, void *b_buf, loff_t pos, unsigned long b_len)
{
	struct zz9000_platform_data *data = dev->data;
	int ret = 0, i;

	pr_debug("read pos %lld len %ld\n", pos, b_len);

	zz_writew(b_len >> SECTOR_SHIFT, MNTZZ_USB_STATUS);	//num_blocks;
	zz_writew((pos >> SECTOR_SHIFT) >> 16, MNTZZ_USB_RX_HI);
	zz_writew((pos >> SECTOR_SHIFT) & 0xffff, MNTZZ_USB_RX_LO);

	if (zz_readw(MNTZZ_USB_STATUS) != (b_len >> SECTOR_SHIFT))
		printk(KERN_WARNING "zz9000: USB: unexpected read status=%d(%d)\n",
			zz_readw(MNTZZ_USB_STATUS), (uint32_t)(b_len >> SECTOR_SHIFT));

	for (i = 0; i < (b_len >> SECTOR_SHIFT); i++) {
		zz_writew(i, MNTZZ_USB_BUFSEL);
		memcpy(b_buf + (i << SECTOR_SHIFT), data->usb_mem, 512);
	}

	return ret;
}

static int zz9000_usb_write(zz9000_usb_storage_t *dev, void *b_buf, loff_t pos, unsigned long b_len)
{
	struct zz9000_platform_data *data = dev->data;
	int ret = 0, i;

	pr_debug("write pos %lld len %ld\n", pos, b_len);

	for (i = 0; i < (b_len >> SECTOR_SHIFT); i++) {
		zz_writew(i, MNTZZ_USB_BUFSEL);
		memcpy(data->usb_mem, b_buf + (i << SECTOR_SHIFT), 512);
	}

	zz_writew(b_len >> SECTOR_SHIFT, MNTZZ_USB_STATUS);
	zz_writew((pos >> SECTOR_SHIFT) >> 16, MNTZZ_USB_TX_HI);
	zz_writew((pos >> SECTOR_SHIFT) & 0xffff, MNTZZ_USB_TX_LO);

	if (zz_readw(MNTZZ_USB_STATUS) != (b_len >> SECTOR_SHIFT))
		printk(KERN_WARNING "zz9000: USB: unexpected write status=%d(%d)\n",
			zz_readw(MNTZZ_USB_STATUS), (uint32_t)(b_len >> SECTOR_SHIFT));

	return ret;
}

static int zz9000_usb_request(struct request *rq, unsigned int *nr_bytes)
{
	int ret = 0;
	struct bio_vec bvec;
	struct req_iterator iter;
	zz9000_usb_storage_t *dev = rq->q->queuedata;
	loff_t pos = blk_rq_pos(rq) << SECTOR_SHIFT;
	loff_t dev_size = (loff_t) (dev->capacity << SECTOR_SHIFT);

	pr_debug("zz9000: USB: request start from sector %lld \n",
	       blk_rq_pos(rq));

	rq_for_each_segment(bvec, rq, iter) {
		unsigned long b_len;
		void *b_buf;

	        b_len = bvec.bv_len;
		b_buf = page_address(bvec.bv_page) + bvec.bv_offset;

		if ((pos + b_len) > dev_size)
			b_len = (unsigned long)(dev_size - pos);

		if (rq_data_dir(rq)) {
			pr_debug("zz9000: USB: Write\n");
			ret = zz9000_usb_write(dev, b_buf, pos, b_len);
		} else {
			pr_debug("zz9000: USB: Read\n");
			ret = zz9000_usb_read(dev, b_buf, pos, b_len);
		}
		pos += b_len;
		*nr_bytes += b_len;
	}

	return ret;
}

static blk_status_t zz9000_usb_queue_rq(struct blk_mq_hw_ctx *hctx,
					const struct blk_mq_queue_data *bd)
{
	blk_status_t status = BLK_STS_OK;
	struct request *rq = bd->rq;

	blk_mq_start_request(rq);

	{
		unsigned int nr_bytes = 0;
		if (zz9000_usb_request(rq, &nr_bytes) != 0)
			status = BLK_STS_IOERR;

		pr_debug("zz9000: USB: request process %d bytes\n", nr_bytes);

		if (blk_update_request(rq, status, nr_bytes))
			BUG();
		__blk_mq_end_request(rq, status);
	}

	return BLK_STS_OK;
}

static struct blk_mq_ops zz9000_mq_ops = {
	.queue_rq = zz9000_usb_queue_rq,
};

int zz9000_usb_major = 0;
static struct request_queue *zz9000_usb_queue;
static zz9000_usb_storage_t *zz9000_usb_storage;

int zz9000_usb_init(struct zz9000_platform_data *data)
{
	zz9000_usb_storage_t *dev;
	int ret = 0;
	int usb_block_size = 512;
	unsigned int usb_capacity;
	char cap_str_2[10], cap_str_10[10];

	zz9000_data = data;

	usb_capacity = zz_readl(MNTZZ_USB_CAPACITY);
	if (usb_capacity == 0) {
		printk(KERN_INFO "  USB device not found at boot.\n");
		return 0;
	}

	string_get_size(usb_capacity, usb_block_size,
			STRING_UNITS_2, cap_str_2, sizeof(cap_str_2));
	string_get_size(usb_capacity, usb_block_size,
			STRING_UNITS_10, cap_str_10, sizeof(cap_str_10));

	printk(KERN_INFO
	       "  USB capacity: %d %d-byte logical blocks: (%s GB/%s GiB)\n",
	       usb_capacity, usb_block_size, cap_str_10, cap_str_2);

	/* Register block device */
	if ((zz9000_usb_major = register_blkdev(0, "zzusb")) <= 0)
		return -EIO;

	dev = kzalloc(sizeof(zz9000_usb_storage_t), GFP_KERNEL);
	if (dev == NULL) {
		printk(KERN_WARNING
		       "zz9000: USB: Unable to allocate %zd bytes\n",
		       sizeof(zz9000_usb_storage_t));
		return -ENOMEM;
	}
	dev->data = data;
	zz9000_usb_storage = dev;

	/* Remember capacity in blocks */
	dev->capacity = usb_capacity;

	/* configure tag_set */
	dev->tag_set.cmd_size = sizeof(zz9000_usb_cmd_t);
	dev->tag_set.driver_data = dev;

	zz9000_usb_queue =
	    blk_mq_init_sq_queue(&dev->tag_set, &zz9000_mq_ops, 128,
				 BLK_MQ_F_SHOULD_MERGE);
	if (IS_ERR(zz9000_usb_queue)) {
		ret = PTR_ERR(zz9000_usb_queue);
		printk(KERN_WARNING
		       "zz9000: USB: Unable to allocate and initialize tag set\n");
		return -EIO;
	}
	dev->queue = zz9000_usb_queue;

	dev->queue->queuedata = dev;

	/* Set the hardware sector size and the max number of sectors */
	//blk_queue_hardsect_size(zz9000_usb_queue, usb_block_size);
	blk_queue_logical_block_size(zz9000_usb_queue, usb_block_size);
	blk_queue_max_hw_sectors(zz9000_usb_queue, 32);

	/* Allocate an associated gendisk */
	dev->disk = alloc_disk(16);
	if (dev->disk == NULL) {
		printk(KERN_WARNING "zz9000: USB: Failed to allocate disk\n");
		return -ENOMEM;
	}

	/* Fill in parameters associated with the gendisk */
	dev->disk->flags |= GENHD_FL_REMOVABLE;
	dev->disk->fops = &zz9000_usb_fops;
	dev->disk->queue = zz9000_usb_queue;
	dev->disk->major = zz9000_usb_major;
	dev->disk->first_minor = 0;
	dev->disk->private_data = dev;

	sprintf(dev->disk->disk_name, "zzusb");

	/* Set the capacity of USB storage media in number of sectors */
	set_capacity(dev->disk, usb_capacity);

	/* Add disk to the block I/O subsystem */
	add_disk(dev->disk);

	pr_debug("zz9000: USB block device was initialized.\n");

	return 0;
}

int zz9000_usb_remove(struct zz9000_platform_data *data)
{
	zz9000_usb_storage_t *dev = zz9000_usb_storage;

	if (dev == NULL)
		return 0;

	if (dev->disk)
		del_gendisk(dev->disk);

	if (dev->queue) {
		blk_cleanup_queue(dev->queue);
		dev->queue = NULL;
	}

	if (dev->tag_set.tags)
		blk_mq_free_tag_set(&dev->tag_set);

	if (dev->disk) {
		put_disk(dev->disk);
		dev->disk = NULL;
	}

	kfree(dev);
	zz9000_usb_storage = NULL;

	if (zz9000_usb_major > 0)
		unregister_blkdev(zz9000_usb_major, "zzusb");

	pr_debug("zz9000: USB block device was removed.\n");

	return 0;
}

static int __init zz9000_usb_module_init(void)
{
	struct zz9000_platform_data *data = get_zz9000_data();
	return zz9000_usb_init(data);
}

module_init(zz9000_usb_module_init);

static void __exit zz9000_usb_module_exit(void)
{
	struct zz9000_platform_data *data = get_zz9000_data();
	zz9000_usb_remove(data);
}

module_exit(zz9000_usb_module_exit);

MODULE_DESCRIPTION("MNT ZZ9000 USB driver");
MODULE_AUTHOR("Stefan Reinauer <stefan.reinauer@coreboot.org>");
MODULE_LICENSE("GPL");
