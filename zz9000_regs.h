/*
 * Copyright (c) 2020 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Alain Runa.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *	notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *	notice, this list of conditions and the following disclaimer in the
 *	documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Address space */
#define MNTZZ_REG_BASE			(0x00000000)
#define MNTZZ_REG_SIZE			(0x00002000)
#define MNTZZ_FB_BASE			(0x00010000)
#define MNTZZ_FB_SIZE_ZIII		(0x00ff0000)	/* 16 MB - 64 KB */
#define MNTZZ_FB_SIZE_ZII		(0x003f0000)	/*  4 MB - 64 KB */
#define MNTZZ_RX_BASE			(0x00002000)
#define MNTZZ_RX_SIZE			(0x00002000)
#define MNTZZ_TX_BASE			(0x00008000)
#define MNTZZ_TX_SIZE			(0x00002000)
#define MNTZZ_USB_BASE			(0x0000a000)
#define MNTZZ_USB_SIZE			(0x00001000)

/* Color mode */
#define MNTZZ_COLOR_8BIT		(0)
#define MNTZZ_COLOR_16BIT		(1)
#define MNTZZ_COLOR_32BIT		(2)
#define MNTZZ_COLOR_15BIT		(3)

/* Modes of MNTZZ_MODE */
#define MNTZZ_MODE_1280x720		(0)
#define MNTZZ_MODE_800x600		(1)
#define MNTZZ_MODE_640x480		(2)
#define MNTZZ_MODE_1024x768		(3)
#define MNTZZ_MODE_1280x1024		(4)
#define MNTZZ_MODE_1920x1080		(5)
#define MNTZZ_MODE_720x576p50		(6)
#define MNTZZ_MODE_1920x1080p50		(7)
#define MNTZZ_MODE_720x480		(8)
#define MNTZZ_MODE_640x512		(9)

/* Some registers expect the modes and scale factors shifted */
#define MNTZZ_MODE_COLOR_8BIT		(MNTZZ_COLOR_8BIT  << 8)
#define MNTZZ_MODE_COLOR_16BIT		(MNTZZ_COLOR_16BIT << 8)
#define MNTZZ_MODE_COLOR_32BIT		(MNTZZ_COLOR_32BIT << 8)
#define MNTZZ_MODE_COLOR_15BIT		(MNTZZ_COLOR_15BIT << 8)
#define MNTZZ_MODE_SCALE_0		(0 << 12)
#define MNTZZ_MODE_SCALE_1		(1 << 12)
#define MNTZZ_MODE_SCALE_2		(2 << 12)
#define MNTZZ_MODE_SCALE_3		(3 << 12)

/* Video capture pan */
#define MNTZZ_CAPTURE_PAN_PAL		(0x00e00000)
#define MNTZZ_CAPTURE_PAN_VGA		(0x00e00bd0)

/* Modes of MNTZZ_VIDEOCAP_VMODE */
#define MMNTZ_VMODE_800x600		(1)
#define MNTZZ_VMODE_720x576		(6)

/* Modes of MNTZZ_VIDEO_CAPTURE_MODE */
#define MNTZZ_CAPTURE_OFF		(0)
#define MNTZZ_CAPTURE_ON		(1)

/* Operations of MNTZ_VIDEO_CONTROL_OP */
#define MNTZZ_OP_NOP			(0)
#define MNTZZ_OP_PALETTE		(3)
#define MNTZZ_OP_VSYNC			(5)

/* Bits of MNTZZ_CONFIG */
#define MNTZZ_CONFIG_INT6		(1 << 0)

/* REGISTERS */

/* Config and Video */
#define MNTZZ_HW_VERSION		0x00
#define MNTZZ_MODE			0x02
#define MNTZZ_CONFIG			0x04

#define MNTZZ_SPRITE_X			0x06
#define MNTZZ_SPRITE_Y			0x08

#define MNTZZ_PAN_PTR_HI		0x0a
#define MNTZZ_PAN_PTR_LO		0x0c
#define MNTZZ_VIDEOCAP_VMODE		0x0e

/* Blitter */
#define MNTZZ_BLITTER_X1		0x10
#define MNTZZ_BLITTER_Y1		0x12
#define MNTZZ_BLITTER_X2		0x14
#define MNTZZ_BLITTER_Y2		0x16
#define MNTZZ_BLITTER_ROW_PITCH		0x18
#define MNTZZ_BLITTER_X3		0x1a
#define MNTZZ_BLITTER_Y3		0x1c
#define MNTZZ_BLITTER_RGB_HI		0x1e
#define MNTZZ_BLITTER_RGB_LO		0x20
#define MNTZZ_BLITTER_OP_FILLRECT	0x22
#define MNTZZ_BLITTER_OP_COPYRECT	0x24
#define MNTZZ_BLITTER_OP_FILLTEMPLATE	0x26
#define MNTZZ_BLITTER_SRC_HI		0x28
#define MNTZZ_BLITTER_SRC_LO		0x2a
#define MNTZZ_BLITTER_DST_HI		0x2c
#define MNTZZ_BLITTER_DST_LO		0x2e
#define MNTZZ_BLITTER_COLORMODE		0x30
#define MNTZZ_BLITTER_SRC_PITCH		0x32
#define MNTZZ_BLITTER_RGB2_HI		0x34
#define MNTZZ_BLITTER_RGB2_LO		0x36
#define MNTZZ_BLITTER_OP_P2C		0x38
#define MNTZZ_BLITTER_OP_DRAW_LINE	0x3a
#define MNTZZ_BLITTER_OP_P2D		0x3c
#define MNTZZ_BLITTER_OP_INVERTRECT	0x3e

#define MNTZZ_BLITTER_USER1		0x40
#define MNTZZ_BLITTER_USER2		0x42
#define MNTZZ_BLITTER_USER3		0x44
#define MNTZZ_BLITTER_USER4		0x46

/* Sprite cursor */
#define MNTZZ_SPRITE_BITMAP		0x48
#define MNTZZ_SPRITE_COLORS		0x4a

/* Network */
#define MNTZZ_ETH_TX			0x80
#define MNTZZ_ETH_RX			0x82
#define MNTZZ_ETH_MAC_HI		0x84
#define MNTZZ_ETH_MAC_MD		0x86
#define MNTZZ_ETH_MAC_LO		0x88

/* Board hardware */
#define MNTZZ_FW_VERSION		0xc0

/* USB */
#define MNTZZ_USB_TX_HI			0xd0
#define MNTZZ_USB_TX_LO			0xd2
#define MNTZZ_USB_RX_HI			0xd4
#define MNTZZ_USB_RX_LO			0xd6
#define MNTZZ_USB_STATUS		0xd8
#define MNTZZ_USB_BUFSEL		0xda
#define MNTZZ_USB_CAPACITY		0xdc

/* Monitoring */
#define MNTZZ_TEMPERATURE		0xe0
#define MNTZZ_VOLTAGE_AUX		0xe2
#define MNTZZ_VOLTAGE_INT		0xe4

/* Video Control */
#define MNTZZ_VIDEO_CTRL_DATA_HI	0x1000
#define MNTZZ_VIDEO_CTRL_DATA_LO	0x1002
#define MNTZZ_VIDEO_CTRL_OP		0x1004
#define MNTZZ_VIDEO_CAPTURE_MODE	0x1006


