# MNT ZZ9000 Drivers (Linux/m68k)

More Info: https://mntre.com/zz9000

## Build

Adjust the path to your Linux kernel sources in the Makefile before
starting. On Debian you can install linux-headers-m68k to get the kernel
headers and the necessary Module.symvers file.

Then you can build the drivers out of the tree with

```
$ make
make[1]: Entering directory '/usr/src/linux-source-5.4'
  CC [M]  git/zz9000-linux-m68k/zz9000_core.o
  LD [M]  git/zz9000-linux-m68k/zz9000.o
  CC [M]  git/zz9000-linux-m68k/zz9000_network.o
  CC [M]  git/zz9000-linux-m68k/zz9000_usb.o
  CC [M]  git/zz9000-linux-m68k/zz9000_graphics.o
  Building modules, stage 2.
  MODPOST 4 modules
  CC [M]  git/zz9000-linux-m68k/zz9000.mod.o
  LD [M]  git/zz9000-linux-m68k/zz9000.ko
  CC [M]  git/zz9000-linux-m68k/zz9000_graphics.mod.o
  LD [M]  git/zz9000-linux-m68k/zz9000_graphics.ko
  CC [M]  git/zz9000-linux-m68k/zz9000_network.mod.o
  LD [M]  git/zz9000-linux-m68k/zz9000_network.ko
  CC [M]  git/zz9000-linux-m68k/zz9000_usb.mod.o
  LD [M]  git/zz9000-linux-m68k/zz9000_usb.ko
make[1]: Leaving directory '/usr/src/linux-source-5.4'
```

Now you can install the drivers with

```
make modules_install
```

## Loading the drivers

You can load your drivers e.g. by creating a ZZ9000 specific file in
/etc/modules-load.d:

```
sudo vi /etc/modules-load.d/zz9000.conf
```

```
# /etc/modules-load.d/zz9000
#
# Load ZZ9000 modules automatically at boot
#

zz9000
zz9000-usb
zz9000-network
zz9000-graphics

options zz9000-graphics noaccel
```

On your next boot you should then see

```
[   43.210000] MNT ZZ9000 driver v0.1.5.1 beta
[   43.230000] MNT ZZ9000 Zorro III found at 0x40000000
[   43.250000]   HW version 0
[   43.260000]   FW version 1.5
[   44.940000]   USB capacity: 7669824 512-byte logical blocks: (3.93 GB GB/3.66 GiB GiB)
[   45.490000]  zzusb: zzusb1
[   47.440000]   ZZ9000 Ethernet eth0 MAC ADDR: 68:82:f2:01:00:54
[   49.080000] zz9000 zorro0: framebuffer at 0x40010000, 0x500000 bytes, mapped to 0xfe68b644
[   49.100000] zz9000 zorro0: format=16bit RGB(5:6:5), mode=1280x720x16, linelength=2560
[   49.580000] Console: switching to colour frame buffer device 160x45
[   51.760000] zz9000 zorro0: fb1: zz9000fb registered!
```

## Graphics

Boot the kernel with video=amifb:off. The driver switches to
1280x720-60@16 by default, but supports all resolutions that the card
supports (non-16bit modes currently untested)

## Network

The network driver currently expects the MAC address to be set up by
AmigaOS already.

## USB

Only one USB drive is supported at the moment, and the drive can not be
hot-plugged.

# License / Copyright

Copyright (C) 2020 Stefan Reinauer <stefan.reinauer@coreboot.org>

Work based on existing Linux drivers, and reusing the register
descriptions from the NetBSD driver.

SPDX-License-Identifier: GPL-2.0
https://spdx.org/licenses/GPL-2.0.html

