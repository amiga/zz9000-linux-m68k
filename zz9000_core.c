/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

#include <linux/console.h>
#include <linux/module.h>
#include <linux/zorro.h>
#include <asm/amigahw.h>
#include "zz9000.h"

static struct zz9000_platform_data *zz9000_data = NULL;

struct zz9000_platform_data *get_zz9000_data(void)
{
	return zz9000_data;
}
EXPORT_SYMBOL_GPL(get_zz9000_data);

static ssize_t zz9000_show(struct kobject *kobj, struct kobj_attribute *attr,
                      char *buf)
{
	int pos = 0;
	uint16_t temp;

	pos += sprintf(buf + pos, "MNT ZZ9000 Zorro %s found at 0x%08x\n",
	       is_zorro3(zz9000_data->zdev) ? "III" : "II", zz9000_data->zdev->resource.start);
	pos += sprintf(buf + pos, "  driver version: %s\n", ZZ9000_VERSION);
	pos += sprintf(buf + pos, "  HW version:     %d\n", zz_readw(MNTZZ_HW_VERSION));
	pos += sprintf(buf + pos, "  FW version:     %d.%d\n", zz_readw(MNTZZ_FW_VERSION) >> 8,
			zz_readw(MNTZZ_FW_VERSION) & 0xff);
	temp = zz_readw(MNTZZ_TEMPERATURE);
	pos += sprintf(buf + pos, "  Temperature:    %d.%02d°C\n", temp / 10, temp % 10);
	temp = zz_readw(MNTZZ_VOLTAGE_AUX);
	pos += sprintf(buf + pos, "  Voltage Vaux:   %d.%02dV\n", temp / 100, temp % 100);
	temp = zz_readw(MNTZZ_VOLTAGE_INT);
	pos += sprintf(buf + pos, "  Voltage Vint:   %d.%02dV\n", temp / 100, temp % 100);

	printk(KERN_INFO "Bytes read: %d\n", pos);
}

static ssize_t zz9000_store(struct kobject *kobj, struct kobj_attribute *attr,
                      const char *buf, size_t count)
{
	// Nothing to do here.
	return count;
}

static struct kobj_attribute zz9000_info_attribute =
	__ATTR(info, 0664, zz9000_show,  zz9000_store);

static void zz9000_remove(struct zorro_dev *zdev)
{
	struct zz9000_platform_data *zz9000_data = zorro_get_drvdata(zdev);
	uint32_t board = zdev->resource.start;

	if (zz9000_data->sysfs_root)
		root_device_unregister(zz9000_data->sysfs_root);

	if (zz9000_data->registers)
		iounmap(zz9000_data->registers);
	if (zz9000_data->eth_rx_mem)
		iounmap(zz9000_data->eth_rx_mem);
	if (zz9000_data->eth_tx_mem)
		iounmap(zz9000_data->eth_tx_mem);
	if (zz9000_data->usb_mem)
		iounmap(zz9000_data->usb_mem);
	if (zz9000_data->fb_mem)
		iounmap(zz9000_data->fb_mem);

	if (zz9000_data->res[0])
		release_mem_region(board + MNTZZ_REG_BASE, MNTZZ_REG_SIZE);
	if (zz9000_data->res[1])
		release_mem_region(board + MNTZZ_RX_BASE, MNTZZ_RX_SIZE);
	if (zz9000_data->res[2])
		release_mem_region(board + MNTZZ_TX_BASE, MNTZZ_TX_SIZE);
	if (zz9000_data->res[3])
		release_mem_region(board + MNTZZ_USB_BASE, MNTZZ_USB_SIZE);
	if (zz9000_data->res[4])
		release_mem_region(board + MNTZZ_FB_BASE, zz9000_data->fb_size);
}

static int zz9000_probe(struct zorro_dev *zdev,
			const struct zorro_device_id *ent)
{
	int ret = 0;
	uint16_t temp;
	uint32_t board = zdev->resource.start;

	printk(KERN_INFO "MNT ZZ9000 driver v%s\n", ZZ9000_VERSION);

	zz9000_data =
	    devm_kzalloc(&zdev->dev, sizeof(*zz9000_data), GFP_KERNEL);
	if (!zz9000_data) {
		ret = -ENOMEM;
		goto out;
	}
	zorro_set_drvdata(zdev, zz9000_data);

	/* How much memory is visible? */
	zz9000_data->fb_size = is_zorro3(zdev) ? MNTZZ_FB_SIZE_ZIII : MNTZZ_FB_SIZE_ZII;

	zz9000_data->res[0] = request_mem_region(board + MNTZZ_REG_BASE, MNTZZ_REG_SIZE, "ZZ9000 Registers");
	if (!zz9000_data->res[0]) {
		dev_err(&zdev->dev, "Could not reserve register region.\n");
		ret = -ENXIO;
		goto cleanup;
	}

	zz9000_data->res[1] = request_mem_region(board + MNTZZ_RX_BASE, MNTZZ_RX_SIZE, "ZZ9000 Network RX");
	if (!zz9000_data->res[1]) {
		dev_err(&zdev->dev, "Could not reserve network RX region.\n");
		ret = -ENXIO;
		goto cleanup;
	}

	zz9000_data->res[2] = request_mem_region(board + MNTZZ_TX_BASE, MNTZZ_TX_SIZE, "ZZ9000 Network TX");
	if (!zz9000_data->res[2]) {
		dev_err(&zdev->dev, "Could not reserve network TX region.\n");
		ret = -ENXIO;
		goto cleanup;
	}

	zz9000_data->res[3] = request_mem_region(board + MNTZZ_USB_BASE, MNTZZ_USB_SIZE, "ZZ9000 USB");
	if (!zz9000_data->res[3]) {
		dev_err(&zdev->dev, "Could not reserve USB buffer region.\n");
		ret = -ENXIO;
		goto cleanup;
	}

	zz9000_data->res[4] = request_mem_region(board + MNTZZ_FB_BASE, zz9000_data->fb_size, "ZZ9000 Framebuffer");
	if (!zz9000_data->res[4]) {
		dev_err(&zdev->dev, "Could not reserve framebuffer region.\n");
		ret = -ENXIO;
		goto cleanup;
	}

	zz9000_data->registers = z_ioremap(board + MNTZZ_REG_BASE, MNTZZ_REG_SIZE);
	zz9000_data->eth_rx_mem = z_ioremap(board + MNTZZ_RX_BASE, MNTZZ_RX_SIZE);
	zz9000_data->eth_tx_mem = z_ioremap(board + MNTZZ_TX_BASE, MNTZZ_TX_SIZE);
	zz9000_data->usb_mem = z_ioremap(board + MNTZZ_USB_BASE, MNTZZ_USB_SIZE);
	/* access the videomem with writethrough cache */
	zz9000_data->fb_mem = ioremap_wt(board + MNTZZ_FB_BASE, zz9000_data->fb_size);
	zz9000_data->zdev = zdev;

	if (!zz9000_data->registers || !zz9000_data->eth_rx_mem ||
			!zz9000_data->eth_tx_mem || !zz9000_data->usb_mem ||
			!zz9000_data->fb_mem) {
		dev_err(&zdev->dev, "Could not ioremap device memory.\n");
		ret = -ENXIO;
		goto cleanup;
	}

	printk(KERN_INFO "MNT ZZ9000 Zorro %s found at 0x%08x\n",
	       is_zorro3(zdev) ? "III" : "II", board);
	printk(KERN_INFO "  HW version %d\n", zz_readw(MNTZZ_HW_VERSION));
	printk(KERN_INFO "  FW version %d.%d\n", zz_readw(MNTZZ_FW_VERSION) >> 8,
			zz_readw(MNTZZ_FW_VERSION) & 0xff);
	temp = zz_readw(MNTZZ_TEMPERATURE);
	printk(KERN_INFO "  Temperature %d.%02d degree C\n", temp / 10, temp % 10);
	temp = zz_readw(MNTZZ_VOLTAGE_AUX);
	printk(KERN_INFO "  Voltage Vaux %d.%02dV\n", temp / 100, temp % 100);
	temp = zz_readw(MNTZZ_VOLTAGE_INT);
	printk(KERN_INFO "  Voltage Vint %d.%02dV\n", temp / 100, temp % 100);

	zz9000_data->sysfs_root = root_device_register("zz9000");
	if(!zz9000_data->sysfs_root) {
		ret = -ENOMEM;
		goto cleanup;
	}

	ret = sysfs_create_file(&zz9000_data->sysfs_root->kobj,
			&zz9000_info_attribute.attr);
	if (ret) {
		pr_debug("Failed to create /sys/devices/zz9000/info\n");
	}


	if (!ret)
		return 0;

cleanup:
	zz9000_remove(zdev);
out:
	return ret;
}

static const struct zorro_device_id zz9000_zorro_tbl[] = {
	{ZORRO_PROD_MNT_ZZ9000_ZII},
	{ZORRO_PROD_MNT_ZZ9000_ZIII},
	{0}
};

MODULE_DEVICE_TABLE(zorro, zz9000_zorro_tbl);

static struct zorro_driver zz9000_driver = {
	.name = "zz9000",
	.id_table = zz9000_zorro_tbl,
	.probe = zz9000_probe,
	.remove = zz9000_remove,
};

static int __init zz9000_init(void)
{
	return zorro_register_driver(&zz9000_driver);
}

module_init(zz9000_init);

static void __exit zz9000_exit(void)
{
	zorro_unregister_driver(&zz9000_driver);
}

module_exit(zz9000_exit);

MODULE_DESCRIPTION("MNT ZZ9000 core driver");
MODULE_AUTHOR("Stefan Reinauer <stefan.reinauer@coreboot.org>");
MODULE_LICENSE("GPL");
